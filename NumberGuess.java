import java.util.Random;
import java.util.Scanner;

public class NumberGuess {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Random random = new Random();
        System.out.println("Let the game begin!");
        System.out.print("Enter your name: ");
        String name = scan.nextLine();
        System.out.println("Hello, " + name + "! Try to guess the number.");
        int randomNumber = random.nextInt(100);
        while (true){

            System.out.print("Enter your guess: ");
            int guess = scan.nextInt();
            scan.nextLine();
            if (randomNumber<guess) {
                System.out.println("Your number is too big. Please, try again. ");
            }
            else if (randomNumber>guess){
                System.out.println("Your number is too small. Please, try again.");
            }
            else {
                System.out.println("Congratulations, " + name + "!");
                break;
            }
        }
        scan.close();
    }
}
